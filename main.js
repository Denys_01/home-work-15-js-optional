let userNumber = Number(prompt("Please type number"));
function factorial(n) {
  return (n != 1) ? n * factorial(n - 1) : 1;
}
function oneMore() {
    while (isNaN(userNumber) || userNumber == "") {
        userNumber = Number(prompt("Please type number"));
    }
    console.log(factorial(userNumber));
}
oneMore();

